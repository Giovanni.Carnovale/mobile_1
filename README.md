# Homework N1 di Mobile



## Consegna

Design and implement the following system.<br>

●Create the following prefabs, evaluating whether or not to use variants:<br>
-   Large object with a mass of 10.<br>
-   Medium-size object with a mass of 5.<br>
-   Small object with a mass of 1.<br>

●Create a scene with a room (approx 10 times larger than the large object).<br>
●When the scene starts, periodically shot physically-simulated objects in random directions from a couple of fixed points in the room.<br>
●When an object collides with the room or another object, destroy it and create an appropriate number of smaller objects, preserving momentum. Newly created objects should not be destroyed immediately when colliding with “sibling” objects or the ones that caused the original collision.<br>
●Destroy objects that stay still for a certain amount of time (ie. 3 seconds).<br>

