using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallHandler : MonoBehaviour
{
    [Header("GameObjects e Prefab")]

    [SerializeField]
    private GameObject bulletMedium, bulletLittle; //prefab dei proiettili in cui spezzarsi

    [Header("Proprieta")]

    [SerializeField]
    private float invicibilityTime = 3f; //in sec
    [SerializeField]
    private float flashPeriod = 0.1f; //time between flashes during invicnibility
    [SerializeField]
    private float lifeTime = 3f; //time that the object will wait before being destroyed if no collision
    [SerializeField]
    private float xOffset = 10f, yOffset = 10f, zOffset = 10;
    [SerializeField]
    private Color invicibilyColor = Color.white;




    [SerializeField]
    private int numberOfShards = 3;


    private bool invincible;
    private Rigidbody myRB;
    private Renderer renderer;
    private Color defaultColor;
    

    void Start()
    {
        renderer = this.GetComponent<Renderer>();
        defaultColor = renderer.material.color;

        myRB = GetComponent<Rigidbody>();
        invincible = true;
        StartCoroutine(timer());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!invincible)
        {
            for(int i = 0; i<Random.Range(2, numberOfShards); i++)
            {
                GameObject childBullet = null;
                switch (tag)
                {
                    case "Ball Big":
                        childBullet = Instantiate(bulletMedium, transform.position, Quaternion.identity);
                        break;
                    case "Ball Mid":
                        childBullet = Instantiate(bulletLittle, transform.position, Quaternion.identity);
                        break;
                    default:
                        Destroy(this.gameObject);
                        break;
                }
                //Inherit speed
                if (childBullet != null) {
                    Rigidbody childRB = childBullet.GetComponent<Rigidbody>();
                    childRB.velocity = myRB.velocity;
                    childRB.angularVelocity = myRB.angularVelocity;
                }
            
            }


            Destroy(this.gameObject);
        }
    }

    //Timer for invincibility, when I'm not invincible I start the lifetime timer
    private IEnumerator timer()
    {
        if (invincible)
        {
            //StartCoroutine(flash());      //turned off because I don't like the visual effect
            yield return new WaitForSeconds(invicibilityTime);
            invincible = false;
            StartCoroutine(lifeTimer());
        }
    }

    //Flashes during invincibility
    private IEnumerator flash()
    {
        while (invincible)
        {
            if (renderer.material.color == defaultColor)
                renderer.material.color = invicibilyColor;
            else
                renderer.material.color = defaultColor;

            yield return new WaitForSeconds(flashPeriod);
        }

        renderer.material.color = defaultColor;
        yield return null;
    }

    //Timer that will call destruction of the object. If the object manages to collide to something it will be destroyed by the collision
    private IEnumerator lifeTimer()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(this.gameObject);
    }
}
