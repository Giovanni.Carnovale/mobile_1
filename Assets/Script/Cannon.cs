using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [Header("GameObjects e Prefab")]

    [SerializeField]
    private GameObject muzzle; //Gives position from where to shoot balls
    [SerializeField]
    private GameObject bulletPrefab;

    [Header("Proprieta")]

    [SerializeField]
    private float timeBetweenFiring = 3f; //in sec
    [SerializeField]
    private float randomTimeOffset = 0.5f; //in sec
    [SerializeField]
    private float firingForce = 5f;
    [SerializeField]
    private float xDirectionOffset = 2;
    [SerializeField]
    private float yDirectionOffset = 2;
    [SerializeField]
    private float firingRotation = 5f;

    private bool readyToShoot;
    private float timeMultiplier;
    private bool enabled;

    // Start is called before the first frame update
    void Awake()
    {
        if(muzzle == null)
        {
            muzzle = transform.Find("muzzle").gameObject;
        }
        readyToShoot = true;
        enabled = false;
        timeMultiplier = 1f;
        StartCoroutine(timer());
    }

    void FixedUpdate()
    {
        if (readyToShoot && enabled)
        {
            GameObject bullet = GameObject.Instantiate(bulletPrefab, muzzle.transform.position, Quaternion.identity, this.transform);
            Rigidbody bulletRB = bullet.GetComponent<Rigidbody>();

            bulletRB.velocity = muzzle.transform.forward * firingForce 
                + xDirectionOffset * Random.Range(-1, 1) * muzzle.transform.right 
                + yDirectionOffset * Random.Range(-1, 1) * muzzle.transform.up;

            bulletRB.angularVelocity = firingRotation * (new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)));
            
            readyToShoot = false;
            StartCoroutine(timer());
        }
    }

    public void setEnabled(bool value)
    {
        this.enabled = value;
    }

    public void setTimeMultiplier(float time)
    {
        Debug.Log("Value changed to " + 1/time);
        this.timeMultiplier = 1/time;
    }

    private IEnumerator timer()
    {
        if (!readyToShoot)
        {
            float time = timeBetweenFiring + (Random.Range(-1, 1) * randomTimeOffset);
            time *= timeMultiplier;
            yield return new WaitForSeconds(time);
            readyToShoot = true;
        }
    }
}
