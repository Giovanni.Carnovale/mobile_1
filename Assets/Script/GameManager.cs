using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Image blackSquare; //Black square for fade out on exit
    [SerializeField]
    private float fadeoutTime = 1.2f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine("fadeout");
        } else if (Input.GetKeyUp(KeyCode.Escape))
        {
            StopCoroutine("fadeout");
            blackSquare.color = new Color(0, 0, 0, 0);
        }
    }

    private IEnumerator fadeout()
    {
        for(float i = 0; i<fadeoutTime; i += Time.deltaTime)
        {
            blackSquare.color = new Color(0, 0, 0, Mathf.Lerp(0, 1, i/fadeoutTime));
            yield return null;
        }
        Debug.Log("Quitting");
        Application.Quit();
    }
}
