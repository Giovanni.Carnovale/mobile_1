using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldParser : MonoBehaviour
{
    [SerializeField]
    private GameObject generator, slider;

    private InputField inputField;

    private void Awake()
    {
        this.inputField = GetComponent<InputField>();
    }
    public void updateValues()
    {
        float value = float.Parse(inputField.text);

        value = Mathf.Clamp(value, 0.5f, 2f);
        updateText(value);

        generator.GetComponent<Cannon>().setTimeMultiplier(value); //This is redundant
        slider.GetComponent<Slider>().value = value;
    }

    public void updateText(float value) {
        inputField.text = "" + value;
    }
}
